package com.ruyuan.o2o.groupbuy.store.filter;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录拦截器
 * <p>
 * 模拟过滤拦截运营管理平台账号登录成功
 *
 * @author ming qian
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        //  模拟验证登录标记成功
        return Boolean.TRUE;
    }
}
