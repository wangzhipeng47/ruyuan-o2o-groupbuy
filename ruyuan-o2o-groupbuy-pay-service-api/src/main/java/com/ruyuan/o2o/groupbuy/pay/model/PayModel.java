package com.ruyuan.o2o.groupbuy.pay.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 支付服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class PayModel implements Serializable {
    private static final long serialVersionUID = 1889165858693867665L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 支付id
     */
    private Integer payId;

    /**
     * 消费者用户id
     */
    private Integer userId;

    /**
     * 门店id
     */
    private Integer storeId;

    /**
     * 支付状态 0 支付成功 1 支付失败
     */
    private Integer payStatus;

    /**
     * 支付渠道 0 会员余额 1 银联卡 2 支付宝 3 微信
     */
    private Integer payChannel;

    /**
     * 支付金额
     */
    private Double payPrice;

    /**
     * 支付时间
     */
    private String payTime;
}