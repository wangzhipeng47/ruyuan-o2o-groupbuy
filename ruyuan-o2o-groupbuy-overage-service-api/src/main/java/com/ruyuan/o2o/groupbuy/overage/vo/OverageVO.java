package com.ruyuan.o2o.groupbuy.overage.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 充值服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "充值服务VO类")
@Getter
@Setter
@ToString
public class OverageVO implements Serializable {
    private static final long serialVersionUID = -4989723767188566898L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 门店id
     */
    @ApiModelProperty("门店id")
    private Integer storeId;

    /**
     * 消费者在门店本次充值余额
     */
    @ApiModelProperty("消费者在门店本次充值余额")
    private Double overageAdd;

    /**
     * 消费者在门店总余额
     */
    @ApiModelProperty("消费者在门店总余额")
    private Double overageTotal;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private String updateTime;
}