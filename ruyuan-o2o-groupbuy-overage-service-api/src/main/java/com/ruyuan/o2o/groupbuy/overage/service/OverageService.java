package com.ruyuan.o2o.groupbuy.overage.service;

import com.ruyuan.o2o.groupbuy.overage.vo.OverageVO;

/**
 * 充值服务service组件接口
 *
 * @author ming qian
 */
public interface OverageService {
    /**
     * 创建充值
     *
     * @param overageVO 充值vo
     */
    void save(OverageVO overageVO);

    /**
     * 查询余额
     *
     * @param userId  消费者用户id
     * @param storeId 门店id
     * @return 余额
     */
    Double findOverage(Integer userId, Integer storeId);
}
