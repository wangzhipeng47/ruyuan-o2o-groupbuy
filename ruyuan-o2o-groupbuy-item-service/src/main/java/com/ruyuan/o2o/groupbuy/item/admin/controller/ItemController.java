package com.ruyuan.o2o.groupbuy.item.admin.controller;

import com.ruyuan.o2o.groupbuy.item.service.ItemService;
import com.ruyuan.o2o.groupbuy.item.vo.ItemVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/item")
@Slf4j
@Api(tags = "商品管理")
public class ItemController {

    @Reference(version = "1.0.0", interfaceClass = ItemService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private ItemService itemService;

    /**
     * 创建商品
     *
     * @param itemVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("创建商品")
    public Boolean save(@RequestBody ItemVO itemVO) {
        itemService.save(itemVO);
        log.info("创建商品:{} 完成", itemVO.getItemName());
        return Boolean.TRUE;
    }

    /**
     * 更新商品
     *
     * @param itemVO 前台表单
     * @return
     */
    @PostMapping("/update")
    @ApiOperation("更新商品")
    public Boolean update(@RequestBody ItemVO itemVO) {
        itemService.update(itemVO);
        log.info("更新商品:{} 完成", itemVO.getItemName());
        return Boolean.TRUE;
    }

    /**
     * 下架商品
     *
     * @param itemId 商品id
     * @return
     */
    @PostMapping("/close/{itemId}")
    @ApiOperation("下架商品")
    public Boolean offLine(@PathVariable("itemId") Integer itemId) {
        itemService.offLine(itemId);
        log.info("商品:{} 下架完成", itemId);
        return Boolean.TRUE;
    }

    /**
     * 上架商品
     *
     * @param itemId 商品id
     * @return
     */
    @PostMapping("/open/{itemId}")
    @ApiOperation("上架商品")
    public Boolean open(@PathVariable("itemId") Integer itemId) {
        itemService.onLine(itemId);
        log.info("商品:{} 上架完成", itemId);
        return Boolean.TRUE;
    }

    /**
     * 分页查询商品列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询商品列表")
    public List<ItemVO> page(Integer pageNum, Integer pageSize) {
        List<ItemVO> itemVoS = itemService.listByPage(pageNum, pageSize);
        return itemVoS;
    }

    /**
     * 根据id查询商品
     *
     * @param itemId 商品id
     * @return
     */
    @GetMapping("/{itemId}")
    @ApiOperation("查询商品详情")
    public ItemVO page(@PathVariable("itemId") Integer itemId) {
        ItemVO itemVO = itemService.findById(itemId);
        return itemVO;
    }

    /**
     * 删除商品
     *
     * @param itemVO 前台表单
     * @return
     */
    @PostMapping("/delete")
    @ApiOperation("删除商品")
    public Boolean delete(@RequestBody ItemVO itemVO) {
        itemService.delete(itemVO.getItemId());
        log.info("删除商品:{} 完成", itemVO.getItemName());
        return Boolean.TRUE;
    }
}
