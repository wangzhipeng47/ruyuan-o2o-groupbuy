package com.ruyuan.o2o.groupbuy.account.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 运营端平台商户角色实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class RoleModel {
    /**
     * 角色id
     */
    private Integer roleId;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色类型
     */
    private Integer roleType;

}
