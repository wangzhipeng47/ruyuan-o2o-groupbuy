package com.ruyuan.o2o.groupbuy.order.admin.controller;

import com.ruyuan.o2o.groupbuy.order.service.OrderService;
import com.ruyuan.o2o.groupbuy.order.vo.OrderVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/order")
@Slf4j
@Api(tags = "运营平台订单管理")
public class OrderController {

    @Reference(version = "1.0.0", interfaceClass = OrderService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private OrderService orderService;

    /**
     * 分页查询订单列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询订单列表")
    public List<OrderVO> page(Integer pageNum, Integer pageSize) {
        List<OrderVO> orderVoS = orderService.listByPage(pageNum, pageSize);
        return orderVoS;
    }

    /**
     * 根据id查询订单
     *
     * @param orderId 订单id
     * @return
     */
    @GetMapping("/{orderId}")
    @ApiOperation("查询订单详情")
    public OrderVO page(@PathVariable("orderId") String orderId) {
        OrderVO orderVO = orderService.findById(orderId);
        return orderVO;
    }

}
