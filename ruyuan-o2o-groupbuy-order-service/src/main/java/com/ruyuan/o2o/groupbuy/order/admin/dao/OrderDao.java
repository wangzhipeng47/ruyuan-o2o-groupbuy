package com.ruyuan.o2o.groupbuy.order.admin.dao;

import com.ruyuan.o2o.groupbuy.order.model.OrderModel;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 订单服务DAO
 *
 * @author ming qian
 */
@Repository
public interface OrderDao {
    /**
     * 生成订单
     *
     * @param orderModel
     */
    void save(OrderModel orderModel);

    /**
     * 分页查询订单
     *
     * @return
     */
    List<OrderModel> listByPage();

    /**
     * 根据id查询订单
     *
     * @param orderId 订单id
     * @return
     */
    OrderModel findById(String orderId);

}