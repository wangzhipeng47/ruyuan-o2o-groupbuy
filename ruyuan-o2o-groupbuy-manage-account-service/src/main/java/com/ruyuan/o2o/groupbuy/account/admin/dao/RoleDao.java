package com.ruyuan.o2o.groupbuy.account.admin.dao;

import com.ruyuan.o2o.groupbuy.account.model.RoleModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色服务DAO
 *
 * @author ming qian
 */
@Repository
public interface RoleDao {

    /**
     * 创建角色
     *
     * @param roleModel 角色model
     */
    void save(RoleModel roleModel);

    /**
     * 更新角色
     *
     * @param roleModel 角色model
     */
    void update(RoleModel roleModel);

    /**
     * 分页查询角色
     *
     * @return
     */
    List<RoleModel> listByPage();

    /**
     * 根据id查询角色
     *
     * @param roleId 角色id
     * @return
     */
    RoleModel findById(Integer roleId);
}
