package com.ruyuan.o2o.groupbuy.excel.admin.controller;

import com.ruyuan.o2o.groupbuy.excel.service.ExcelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 报表管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/excel")
@Slf4j
@Api(tags = "报表管理")
public class ExcelController {

    @Reference(version = "1.0.0", interfaceClass = ExcelService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private ExcelService excelService;

    /**
     * 手动拉取报表
     */
    @GetMapping("/pull")
    @ApiOperation("手动拉取报表")
    public void pull() {
        excelService.excel();
    }
}
