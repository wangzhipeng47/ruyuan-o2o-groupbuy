package com.ruyuan.o2o.groupbuy.item.admin.dao;

import com.ruyuan.o2o.groupbuy.item.model.ItemModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 商品服务DAO
 *
 * @author ming qian
 */
@Repository
public interface ItemDao {

    /**
     * 创建商品
     *
     * @param itemModel
     */
    void save(ItemModel itemModel);

    /**
     * 更新商品
     *
     * @param itemModel
     */
    void update(ItemModel itemModel);

    /**
     * 下架商品
     *
     * @param itemId
     */
    void offLine(Integer itemId);

    /**
     * 上架商品
     *
     * @param itemId
     */
    void onLine(Integer itemId);

    /**
     * 分页查询商品
     *
     * @return
     */
    List<ItemModel> listByPage();

    /**
     * 根据id查询商品
     *
     * @param itemId 商品id
     * @return
     */
    ItemModel findById(Integer itemId);

    /**
     * 删除商品
     *
     * @param itemId
     */
    void delete(Integer itemId);

}
