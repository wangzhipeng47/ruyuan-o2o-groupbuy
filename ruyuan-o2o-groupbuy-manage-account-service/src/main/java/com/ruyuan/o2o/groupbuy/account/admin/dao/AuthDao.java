package com.ruyuan.o2o.groupbuy.account.admin.dao;

import com.ruyuan.o2o.groupbuy.account.model.AuthModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 权限服务DAO
 *
 * @author ming qian
 */
@Repository
public interface AuthDao {
    /**
     * 创建权限
     *
     * @param authModel 权限model
     */
    void save(AuthModel authModel);

    /**
     * 分页查询权限
     *
     * @return
     */
    List<AuthModel> listByPage();

    /**
     * 删除权限
     *
     * @param authId 权限id
     * @return 权限model
     */
    AuthModel findAuthById(Integer authId);
}
