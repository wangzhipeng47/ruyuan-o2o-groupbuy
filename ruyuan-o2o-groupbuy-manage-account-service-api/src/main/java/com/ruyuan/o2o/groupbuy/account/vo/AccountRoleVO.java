package com.ruyuan.o2o.groupbuy.account.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 账号绑定角色VO类
 *
 * @author ming qian
 */
@ApiModel(value = "账号绑定角色VO类")
@Getter
@Setter
@ToString
public class AccountRoleVO implements Serializable {
    private static final long serialVersionUID = -1935583366506067405L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private Integer adminId;

    /**
     * 角色id
     */
    @ApiModelProperty("角色id")
    private Integer roleId;
}
