package com.ruyuan.o2o.groupbuy.account.admin.dao;

import com.ruyuan.o2o.groupbuy.account.model.AccountRoleModel;
import org.springframework.stereotype.Repository;

/**
 * 绑定用户角色DAO
 *
 * @author ming qian
 */
@Repository
public interface AccountRoleDao {

    /**
     * 用户绑定角色
     *
     * @param accountRoleModel 绑定关系model
     * @return
     */
    Boolean accountBindRole(AccountRoleModel accountRoleModel);
}
