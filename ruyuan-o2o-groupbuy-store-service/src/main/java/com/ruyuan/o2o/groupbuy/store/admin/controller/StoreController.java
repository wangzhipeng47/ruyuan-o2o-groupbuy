package com.ruyuan.o2o.groupbuy.store.admin.controller;

import com.ruyuan.o2o.groupbuy.store.service.StoreService;
import com.ruyuan.o2o.groupbuy.store.vo.StoreVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/store")
@Slf4j
@Api(tags = "门店管理")
public class StoreController {

    @Reference(version = "1.0.0", interfaceClass = StoreService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private StoreService storeService;

    /**
     * 更新门店
     *
     * @param storeVO 前台表单
     * @return
     */
    @PostMapping("/update")
    @ApiOperation("更新门店信息")
    public String update(@RequestBody StoreVO storeVO) {
        if (!storeService.update(storeVO)) {
            return "failed";
        }
        log.info("更新门店:{} 完成", storeVO.getStoreName());
        return "success";
    }

    /**
     * 分页查询门店列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询门店列表")
    public List<StoreVO> page(Integer pageNum, Integer pageSize) {
        List<StoreVO> storeVoS = storeService.listByPage(pageNum, pageSize);
        return storeVoS;
    }

    /**
     * 根据id查询门店
     *
     * @param storeId 门店id
     * @return
     */
    @GetMapping("/{storeId}")
    @ApiOperation("查询门店详情")
    public StoreVO page(@PathVariable("storeId") Integer storeId) {
        StoreVO storeVO = storeService.findById(storeId);
        return storeVO;
    }

}
