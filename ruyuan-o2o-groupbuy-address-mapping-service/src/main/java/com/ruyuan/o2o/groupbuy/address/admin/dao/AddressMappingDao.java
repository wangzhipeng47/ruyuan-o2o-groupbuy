package com.ruyuan.o2o.groupbuy.address.admin.dao;

import com.ruyuan.o2o.groupbuy.address.model.AddressMappingModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 省市区映射服务DAO
 *
 * @author ming qian
 */
@Repository
public interface AddressMappingDao {
    /**
     * 创建省市区映射
     *
     * @param addressMappingModel
     */
    void save(AddressMappingModel addressMappingModel);

    /**
     * 更新省市区映射
     *
     * @param addressMappingModel
     */
    void update(AddressMappingModel addressMappingModel);

    /**
     * 分页查询省市区映射
     *
     * @return
     */
    List<AddressMappingModel> listByPage();

    /**
     * 查询省名称
     *
     * @param provinceId 省id
     * @return
     */
    String findProvinceName(Integer provinceId);


    /**
     * 查询市名称
     *
     * @param cityId 市id
     * @return
     */
    String findCityName(Integer cityId);

    /**
     * 查询区名称
     *
     * @param districtId 区id
     * @return
     */
    String findDistrictName(Integer districtId);
}