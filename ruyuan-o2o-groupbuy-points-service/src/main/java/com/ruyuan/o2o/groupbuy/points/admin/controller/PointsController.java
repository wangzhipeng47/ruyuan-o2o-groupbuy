package com.ruyuan.o2o.groupbuy.points.admin.controller;

import com.ruyuan.o2o.groupbuy.points.service.PointsService;
import com.ruyuan.o2o.groupbuy.points.vo.PointsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 积分管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/points")
@Slf4j
@Api(tags = "积分管理")
public class PointsController {

    @Reference(version = "1.0.0", interfaceClass = PointsService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private PointsService pointsService;

    /**
     * 增加用户积分
     *
     * @param pointsVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("增加用户积分")
    public Boolean save(@RequestBody PointsVO pointsVO) {
        pointsService.save(pointsVO);
        log.info("增加积分完成");
        return Boolean.TRUE;
    }


    /**
     * 分页查询积分列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询积分列表")
    public List<PointsVO> page(Integer pageNum, Integer pageSize) {
        List<PointsVO> pointsVoS = pointsService.listByPage(pageNum, pageSize);
        return pointsVoS;
    }

    /**
     * 根据用户id查询积分
     *
     * @param userId 积分id
     * @return
     */
    @GetMapping("/{userId}")
    @ApiOperation("查询积分详情")
    public PointsVO page(@PathVariable("userId") Integer userId) {
        PointsVO pointsVO = pointsService.findByUserId(userId);
        return pointsVO;
    }
}
