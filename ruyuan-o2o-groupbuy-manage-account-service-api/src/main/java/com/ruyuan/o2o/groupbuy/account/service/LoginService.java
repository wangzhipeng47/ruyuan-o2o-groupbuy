package com.ruyuan.o2o.groupbuy.account.service;

/**
 * 运营管理平台账号登录service组件接口
 *
 * @author ming qian
 */
public interface LoginService {
    /**
     * 运营管理平台用户登录
     *
     * @param adminName 管理员名称
     * @param passwd    管理员密码
     * @return
     */
    Boolean login(String adminName, String passwd);
}
