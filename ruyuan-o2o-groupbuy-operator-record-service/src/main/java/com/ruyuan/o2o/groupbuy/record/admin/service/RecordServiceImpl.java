package com.ruyuan.o2o.groupbuy.record.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.record.admin.dao.RecordDao;
import com.ruyuan.o2o.groupbuy.record.model.RecordModel;
import com.ruyuan.o2o.groupbuy.record.service.RecordService;
import com.ruyuan.o2o.groupbuy.record.vo.RecordVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 消费者用户操作记录服务service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = RecordService.class, cluster = "failfast", loadbalance = "roundrobin")
public class RecordServiceImpl implements RecordService {

    @Autowired
    private RecordDao recordDao;

    /**
     * 保存用户记录
     *
     * @param recordVO
     */
    @Override
    public void save(RecordVO recordVO) {
        RecordModel recordModel = new RecordModel();
        BeanMapper.copy(recordVO, recordModel);
        recordModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        recordDao.save(recordModel);
    }
}
