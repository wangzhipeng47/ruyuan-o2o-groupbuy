package com.ruyuan.o2o.groupbuy.points.admin.dao;


import com.ruyuan.o2o.groupbuy.points.model.PointsModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 积分服务DAO
 *
 * @author ming qian
 */
@Repository
public interface PointsDao {
    /**
     * 创建积分
     *
     * @param pointsModel 积分model
     */
    void save(PointsModel pointsModel);

    /**
     * 根据用户id查询积分
     *
     * @param userId 消费者用户id
     * @return 积分model
     */
    PointsModel findByUserId(Integer userId);

    /**
     * 分页查询积分
     *
     * @return 积分model列表
     */
    List<PointsModel> listByPage();

}