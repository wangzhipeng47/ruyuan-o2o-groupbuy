package com.ruyuan.o2o.groupbuy.account.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 权限实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class AuthModel {
    /**
     * 权限id
     */
    private Integer authId;
    /**
     * 权限名称
     */
    private String authName;
    /**
     * 权限url
     */
    private String authUrl;
    /**
     * 父权限主键
     */
    private Integer parentId;
    /**
     * 权限创建时间
     */
    private String createTime;
    /**
     * 权限更新时间
     */
    private String updateTime;

}
