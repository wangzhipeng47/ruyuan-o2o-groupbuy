package com.ruyuan.o2o.groupbuy.shipping.address.admin.controller;

import com.ruyuan.o2o.groupbuy.shipping.address.service.ShippingAddressService;
import com.ruyuan.o2o.groupbuy.shipping.address.vo.ShippingAddressVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 收货地址管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/shipping")
@Slf4j
@Api(tags = "收货地址管理")
public class ShippingAddressController {

    @Reference(version = "1.0.0", interfaceClass = ShippingAddressService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private ShippingAddressService shippingAddressService;

    /**
     * 保存用户收货地址
     *
     * @param shippingAddressVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("保存用户收货地址")
    public Boolean save(@RequestBody ShippingAddressVO shippingAddressVO) {
        shippingAddressService.save(shippingAddressVO);
        log.info("用户保存地址完成");
        return Boolean.TRUE;
    }
}
