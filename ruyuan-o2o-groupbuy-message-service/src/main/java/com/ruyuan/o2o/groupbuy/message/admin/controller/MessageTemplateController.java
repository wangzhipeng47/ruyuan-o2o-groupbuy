package com.ruyuan.o2o.groupbuy.message.admin.controller;

import com.ruyuan.o2o.groupbuy.message.service.MessageTemplateService;
import com.ruyuan.o2o.groupbuy.message.vo.MessageTemplateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 短信模板管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/message/template")
@Slf4j
@Api(tags = "短信模板管理")
public class MessageTemplateController {

    @Reference(version = "1.0.0", interfaceClass = MessageTemplateService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private MessageTemplateService templateService;

    /**
     * 创建短信模板
     *
     * @param messageTemplateVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("创建短信模板")
    public Boolean save(@RequestBody MessageTemplateVO messageTemplateVO) {
        templateService.saveTemplate(messageTemplateVO);
        log.info("创建短信模板完成");
        return Boolean.TRUE;
    }

}
