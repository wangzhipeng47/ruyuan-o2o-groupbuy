package com.ruyuan.o2o.groupbuy.message.service;

import com.ruyuan.o2o.groupbuy.message.vo.MessageTemplateVO;

/**
 * 短信模板service组件接口
 *
 * @author ming qian
 */
public interface MessageTemplateService {

    /**
     * 保存待发送短信
     *
     * @param messageTemplateVO
     */
    void saveTemplate(MessageTemplateVO messageTemplateVO);

    /**
     * 发送短信
     *
     * @param phone 用户手机号
     * @return 发送结果
     */
    Boolean send(String phone);
}
