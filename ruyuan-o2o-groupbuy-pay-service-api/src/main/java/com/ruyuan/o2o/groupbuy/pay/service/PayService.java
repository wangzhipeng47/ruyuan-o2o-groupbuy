package com.ruyuan.o2o.groupbuy.pay.service;

import com.ruyuan.o2o.groupbuy.pay.vo.PayVO;

import java.util.List;

/**
 * 支付服务service组件接口
 *
 * @author ming qian
 */
public interface PayService {
    /**
     * 生成支付信息
     *
     * @param payVO 支付vo
     * @return 生成信息结果
     */
    Boolean save(PayVO payVO);

    /**
     * 分页查询支付流水信息
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<PayVO> listByPage(Integer pageNum, Integer pageSize);

}
