package com.ruyuan.o2o.groupbuy.user.admin.dao;

import com.ruyuan.o2o.groupbuy.user.model.UserModel;
import org.springframework.stereotype.Repository;

/**
 * 消费者用户服务DAO
 *
 * @author ming qian
 */
@Repository
public interface UserDao {
    /**
     * 保存会员用户
     *
     * @param userModel
     */
    void save(UserModel userModel);

    /**
     * 查询用户手机号
     *
     * @param userId 消费者用户id
     * @return
     */
    String findPhoneById(Integer userId);
}