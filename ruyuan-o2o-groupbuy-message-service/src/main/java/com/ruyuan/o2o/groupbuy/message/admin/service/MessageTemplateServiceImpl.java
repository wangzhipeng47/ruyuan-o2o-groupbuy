package com.ruyuan.o2o.groupbuy.message.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.message.admin.dao.MessageTemplateDao;
import com.ruyuan.o2o.groupbuy.message.model.MessageTemplateModel;
import com.ruyuan.o2o.groupbuy.message.service.MessageTemplateService;
import com.ruyuan.o2o.groupbuy.message.vo.MessageTemplateVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理短信模板的service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = MessageTemplateService.class, cluster = "failfast", loadbalance = "roundrobin")
@Slf4j
public class MessageTemplateServiceImpl implements MessageTemplateService {

    @Autowired
    private MessageTemplateDao messageTemplateDao;

    /**
     * 创建短信模板
     *
     * @param messageTemplateVO
     */
    @Override
    public void saveTemplate(MessageTemplateVO messageTemplateVO) {
        MessageTemplateModel messageTemplateModel = new MessageTemplateModel();
        BeanMapper.copy(messageTemplateVO, messageTemplateModel);
        messageTemplateModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        messageTemplateDao.saveMessageTemplate(messageTemplateModel);
    }

    /**
     * 发送短信
     *
     * @return
     */
    @Override
    public Boolean send(String phone) {
        log.info("调用发送短信接口,发送成功:{} ", phone);
        return Boolean.TRUE;
    }
}
