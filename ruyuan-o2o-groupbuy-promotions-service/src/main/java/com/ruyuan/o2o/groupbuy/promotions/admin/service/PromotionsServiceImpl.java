package com.ruyuan.o2o.groupbuy.promotions.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.promotions.admin.dao.PromotionsDao;
import com.ruyuan.o2o.groupbuy.promotions.model.PromotionsModel;
import com.ruyuan.o2o.groupbuy.promotions.service.PromotionsService;
import com.ruyuan.o2o.groupbuy.promotions.vo.PromotionsVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理活动服务的增删改查service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = PromotionsService.class, cluster = "failfast", loadbalance = "roundrobin")
public class PromotionsServiceImpl implements PromotionsService {

    @Autowired
    private PromotionsDao promotionsDao;

    /**
     * 创建活动
     *
     * @param promotionsVO
     */
    @Override
    public void save(PromotionsVO promotionsVO) {
        PromotionsModel promotionsModel = new PromotionsModel();
        BeanMapper.copy(promotionsVO, promotionsModel);
        promotionsModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        promotionsDao.save(promotionsModel);
    }

    /**
     * 更新活动
     *
     * @param promotionsVO
     */
    @Override
    public void update(PromotionsVO promotionsVO) {
        PromotionsModel promotionsModel = new PromotionsModel();
        BeanMapper.copy(promotionsVO, promotionsModel);
        promotionsDao.update(promotionsModel);
    }

    /**
     * 分页查询活动
     *
     * @return
     */
    @Override
    public List<PromotionsVO> listByPage(Integer pageNum, Integer pageSize) {
        List<PromotionsVO> promotionsVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<PromotionsModel> promotionsModels = promotionsDao.listByPage();

        for (PromotionsModel promotionsModel : promotionsModels) {
            PromotionsVO promotionsVO = new PromotionsVO();
            BeanMapper.copy(promotionsModel, promotionsVO);
            promotionsVoS.add(promotionsVO);
        }

        return promotionsVoS;
    }

    /**
     * 根据id查询活动
     *
     * @param promotionId 活动id
     * @return
     */
    @Override
    public PromotionsVO findById(Integer promotionId) {
        PromotionsModel promotionsModel = promotionsDao.findById(promotionId);
        PromotionsVO promotionsVO = new PromotionsVO();
        BeanMapper.copy(promotionsModel, promotionsVO);
        return promotionsVO;
    }

    /**
     * 删除活动
     *
     * @param promotionId
     */
    @Override
    public void delete(Integer promotionId) {
        promotionsDao.delete(promotionId);
    }


    /**
     * 查询当前运行中活动
     *
     * @return
     */
    @Override
    public PromotionsModel findRunningPromotion() {
        PromotionsModel promotionsModel = promotionsDao.findRunningPromotion();
        return promotionsModel;
    }
}
